CCFLAGS    = $(CFLAGS) -O2 -Wall -Wextra -Wformat -Werror -Wno-unused-parameter -Wno-unused-function
PREFIX     = /usr/local

CC         = $(CROSS_COMPILE)cc
AR         = $(CROSS_COMPILE)ar
RANLIB     = $(CROSS_COMPILE)ranlib
STRIP      = $(CROSS_COMPILE)strip -x -R .note -R .comment
INSTALL    = install
LN         = ln -sf
RM         = rm -f

HEADERS   := execinfo.h
SOURCES   := backtrace.c symtab.c unwind.c
OBJECTS   := $(patsubst %.c,%.o,$(SOURCES))

%.o: %.c
	$(CC) $(CCFLAGS) -c $^ -o $@

all: libexecinfo.a

libexecinfo.a: $(OBJECTS)
	$(AR) rcs libexecinfo.a $^
	$(RANLIB) libexecinfo.a

install:
	$(INSTALL) -d -m 755  $(DESTDIR)$(PREFIX)/lib
	$(INSTALL) -d -m 755  $(DESTDIR)$(PREFIX)/include
	$(INSTALL) -m 644 $(HEADERS) $(DESTDIR)$(PREFIX)/include
	$(INSTALL) -m 644 libexecinfo.a $(DESTDIR)$(PREFIX)/lib
	$(INSTALL) -m 644 backtrace.3 $(DESTDIR)$(PREFIX)/share/man/man3/backtrace.3

clean: libexecinfo.a
	rm $(OBJECTS) libexecinfo.a

.PHONY: all clean install
